package edu.exercicios.tratamentoexcecoes.exerciciofixacao.model.excecoes;

public class ContaExcecoes extends Exception {
    public ContaExcecoes(String msg) {
        super(msg);
    }
}
