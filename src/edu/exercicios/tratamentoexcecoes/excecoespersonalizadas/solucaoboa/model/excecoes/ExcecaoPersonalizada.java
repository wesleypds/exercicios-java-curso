package edu.exercicios.tratamentoexcecoes.excecoespersonalizadas.solucaoboa.model.excecoes;

public class ExcecaoPersonalizada extends Exception {
    public ExcecaoPersonalizada(String msg) {
        super(msg);
    }
}
