package edu.exercicios.introducaopoo.contratos.enums;

public enum NivelTrabalhador {
    JUNIOR,
    PLENO,
    SENIOR;
}
