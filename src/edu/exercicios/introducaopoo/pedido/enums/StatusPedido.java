package edu.exercicios.introducaopoo.pedido.enums;

public enum StatusPedido {
    PAGAMENTO_PENDENTE,
    PROCESSANDO,
    ENVIADO,
    ENTREGUE
}
