package edu.exercicios.interfacespoo.alocacaoveiculocominterface.service;

public interface TaxaService {
    double taxa(double quantia);
}
