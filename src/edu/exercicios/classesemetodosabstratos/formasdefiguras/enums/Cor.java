package edu.exercicios.classesemetodosabstratos.formasdefiguras.enums;

public enum Cor {
    PRETO,
    AZUL,
    VERMELHO
}
