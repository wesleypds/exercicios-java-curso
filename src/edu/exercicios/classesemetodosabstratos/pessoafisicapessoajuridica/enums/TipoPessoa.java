package edu.exercicios.classesemetodosabstratos.pessoafisicapessoajuridica.enums;

public enum TipoPessoa {
    JURIDICA,
    FISICA
}
